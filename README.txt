Hi there,

Welcome to the RTL-SDR Repo maintained by Harper Innes
You can visit my webpage at https://686172706572.tech/


I'll be adding a proper deploy script soon so you don't have to fill out 
details in the "config_webrx.py" file and you can simply add your details as the
deploy script runs and it will build the config for you 

I'll also add support different distros so you can simply clone this repo and run it 
((when im less lazy of course))

If you are running this on a Debian based system then 

"Bash DeployDebian.sh"

and it will begin the process of building your container and running it

once finished open your Web Browser and enter http://localhost:8073 as this is the default port the container is set to

##

Direwolf which owrx uses for packet decoding only has support for "FM"
There is no way to support for it to function on HF Upper or Lower Sideband
See Thread: https://github.com/jketterl/openwebrx/issues/95 (Opened since 2020)

On line 45 through 56 Please fill in the WebGUI configuration in "config_webrx.py"

It looks like this 

# ==== Web GUI configuration ====
receiver_name = "[Reciever Name Here]"
receiver_location = "[Recieve Locaion Here]"
receiver_asl = ASL HERE
receiver_admin = "[Reciever Admin Here]"
receiver_gps = {"lat": Lat Here, "lon": Lon Here}
photo_title = "[INSERT TEXT HERE]"
# photo_desc allows you to put pretty much any HTML you like into the receiver description.
# The lines below should give you some examples of what's possible.
photo_desc = """
[Photo Desc Here]
"""

###Ensure that "docker docker-compose" are installed on your machine###

##

Please feel free to email me at "Webmaster[at]Whisper[dot]Cat if you wish to contribute to this repo

***Special Thanks to contributors who have helped on this repo***

-> Rae (VK2GPU) who went to the effort of putting all the VK repeaters into a spreadsheet and formatted in json <-



Good luck and Best DX 

-Harper (Cat on a Teletype)
