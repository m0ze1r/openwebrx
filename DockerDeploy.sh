#!/bin/bash
#
echo 'Welcome to the Auto Docker Deploy Shell'
echo 'We will be setting up a container and calling it "owrx" '
sleep 3
sudo docker create --name=owrx -p 8073:8073 --device /dev/bus/usb -v /home/$USER/openwebrx/:/etc/openwebrx --restart always jketterl/openwebrx
#
echo 'Hopefully the container has built and nothing failed'
sleep 1
echo 'Starting Docker Container for OpenWebRX/RTL-SDR'
sudo docker start owrx
#
#
#
################################################################
# EXPERIMENTAL MODE 
# RUN AS A SEPERATE COMMAND
# ONLY RUN THIS IF YOU KNOW WHAT YOU ARE DOING#
################################################################
##sudo docker run --device /dev/bus/usb -p 8073:8073 -v /home/$USER/openwebrx/:/etc/openwebrx jketterl/openwebrx:stable
##
